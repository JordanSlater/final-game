package com.fcs.Final.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fcs.Final.FinalGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
                config.width = 800;            // set width of window to 800
                config.height = 600;           // set height of window to 600
                config.title = "Twitch Fighters"; //set title
                config.fullscreen = false; //set fullscreen to true
		new LwjglApplication(new FinalGame(), config); 
	}
}
