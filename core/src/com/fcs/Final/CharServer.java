/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcs.Final;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.badlogic.gdx.graphics.Color;
import java.io.IOException;

/**
 *
 * @author slatj2095
 */
public class CharServer {
    // main server variable
    private Server server;
    // player for storing player data
    // for the guest
    private Player globalPlayer;
    // for the host
    private Player localPlayer;
    
    // used for sending the player's colour to a new player
    private boolean responseRequired;
    // what the server says should be done
    private String action;
    
    public CharServer() throws IOException {
        // start without a join player
        responseRequired = false;
        // start with nothing to do about any recieved information
        action = "doNothing";
        // create new players in the player slots (so that they aren't null)
        globalPlayer = new Player(1,1,Color.CLEAR);
        localPlayer = new Player(1,1,Color.CLEAR);
        // create a new server
        server = new Server();
        // start the server
        server.start();
        // TCP and UDP ports
        server.bind(54555, 54777);
        // server adding a listener (thread) for recieving infromation
        server.addListener(new Listener() {
            // if the server recives information
            public void received (Connection connection, Object o) {
                try{
                    // split the information into an array
                    String[] s = ((String)o).split(" ");
                    // if the label(first spot) for the incoming info is sendPlayer
                    if(s[0].equals("sendPlayer:"))
                    {
                        // set the global player's information to the incoming information
                        globalPlayer.setX(Float.parseFloat(s[1]));
                        globalPlayer.setY(Float.parseFloat(s[2]));
                        globalPlayer.setState(globalPlayer.stringToState(s[3]));
                        globalPlayer.setDirection(globalPlayer.stringToDirection(s[4]));
                        globalPlayer.setAction(globalPlayer.stringToAction(s[5]));
                        globalPlayer.setDeathTick(Float.parseFloat(s[6]));
                    }
                    // if the label(first spot) for the incoming info is initialize
                    if(s[0].equals("initialize:"))
                    {
                        // get the player's commenting
                        globalPlayer.setColour(ColourManager.stringToColour(s[1]));
                        // now the host player must send its colour to the new guest player
                        responseRequired = true;
                    }
                    // if the label(first spot) for the incoming info is hit
                    if(s[0].equals("hit:"))
                    {
                        // set the host players coordinates (where it was killed) to the incoming information
                        localPlayer.setX(Float.parseFloat(s[1]));
                        localPlayer.setY(Float.parseFloat(s[2]));
                        // set the server's action to kill the host
                        action = "killHost";
                    }
                } catch (Exception e){}
            }
        });
    }
    // used for getting the guest player's infromation
    public Player getGlobalPlayer() {
        return this.globalPlayer;
    }
    // used for getting the host player's infromation
    public Player getLocalPlayer() {
        return this.localPlayer;
    }
    // used for getting the server's action
    public String getAction() {
        return this.action;
    }
    // used for getting the information of whether the player's colour must be sent to a new guest player
    public boolean getResponseRequired()
    {
        return responseRequired;
    }
    // used for changing the action
    public void setAction(String s) {
        action = s;
    }
    // sending all the specified player's information as a string
    public void SendPlayer(Player player) {
        String playerInfo = "sendPlayer: "+player.getX()+" "+player.getY()+" "+player.getState()+" "
                +player.getDirection()+" "+player.getAction()+" "+player.getDeathTick();
        server.sendToAllUDP(playerInfo);
    }
    // sending(TCP) the specified player's colour as a string and makes the response not required anymore
    public void initialize(Player player)
    {
        String initialize = "initialize: "+ColourManager.ColourToString(player.getColor());
        server.sendToAllTCP(initialize);
        responseRequired = false;
    }
    // sending(TCP) the player's co-ordinates where the player was hit
    public void hitGuest(Player player)
    {
        String hitGuest = "hit: "+player.getX()+" "+player.getY();
        server.sendToAllTCP(hitGuest);
    }
    // sending(TCP) message to restart the match by respawning
    public void respawn()
    {
        server.sendToAllTCP("respawn");
    }
    // retrieving the guest player's colour
    public Color retrieveColour()
    {
        return globalPlayer.getColor();
    }
}
