/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcs.Final;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.badlogic.gdx.graphics.Color;
import java.io.IOException;

/**
 *
 * @author slatj2095
 */
public class CharClient {
    // main client variable
    private Client client;
     // player for storing player data
    // for the guest
    private Player globalPlayer;
    // for the client
    private Player localPlayer;
    // what the cleint says should be done
    private String action;
    
    public CharClient(String ip) throws IOException {
        // start with nothing to do about any recieved information
        action = "doNothing";
        // create new players in the player slots (so that they aren't null)
        globalPlayer = new Player(1,1,Color.CLEAR);
        localPlayer = new Player(1,1,Color.CLEAR);
        // create a new client
        client = new Client();
        // start the client
        client.start();
        // connect to the ip using the specified TCP and UDP ports
        client.connect(5000, ip, 54555, 54777);
        // client adding a listener (thread) for recieving infromation
        client.addListener(new Listener() {
            // if the client recives information
            public void received (Connection connection, Object o) {
                try{
                    // split the information into an array
                    String[] s = ((String)o).split(" ");
                    // if the label(first spot) for the incoming info is sendPlayer
                    if(s[0].equals("sendPlayer:"))
                    {
                        // set the global player's information to the incoming information
                        globalPlayer.setX(Float.parseFloat(s[1]));
                        globalPlayer.setY(Float.parseFloat(s[2]));
                        globalPlayer.setState(globalPlayer.stringToState(s[3]));
                        globalPlayer.setDirection(globalPlayer.stringToDirection(s[4]));
                        globalPlayer.setAction(globalPlayer.stringToAction(s[5]));
                        globalPlayer.setDeathTick(Float.parseFloat(s[6]));
                    }
                    // if the label(first spot) for the incoming info is initialize
                    if(s[0].equals("initialize:"))
                    {
                        // set the incoming player colour to the guest player's colour
                        globalPlayer.setColour(ColourManager.stringToColour(s[1]));
                    }
                    // if the label(first spot) for the incoming info is hit
                    if(s[0].equals("hit:"))
                    {
                        // set the client players coordinates (where it was killed) to the incoming information
                        localPlayer.setX(Float.parseFloat(s[1]));
                        localPlayer.setY(Float.parseFloat(s[2]));
                        // set the client's action to kill the host
                        action = "killGuest";
                    }
                    // if the server said to respawn
                    if(s[0].equals("respawn"))
                    {
                        // set the cation to respawn
                        action = "respawn";
                    }
                } catch (Exception e){}
            }
        });
    }
    // used for getting the host player's infromation
    public Player getGlobalPlayer(){
        return this.globalPlayer;
    }
    // used for getting the guest player's infromation
    public Player getLocalPlayer(){
        return this.localPlayer;
    }
    // used for getting the host player's infromation
    public String getAction() {
        return this.action;
    }
    // used for changing the action
    public void setAction(String a) {
        this.action = a;
    }
    // sending all the specified player's information as a string
    public void SendPlayer(Player player) {
        String playerInfo = "sendPlayer: "+player.getX()+" "+player.getY()+" "+player.getState()+" "
                +player.getDirection()+" "+player.getAction()+" "+player.getDeathTick();
        client.sendUDP(playerInfo);
    }
    // sending(TCP) the specified player's colour as a string
    public void initialize(Player player)
    {
        String initialize = "initialize: "+ColourManager.ColourToString(player.getColor());
        client.sendTCP(initialize);
    }
    // sending(TCP) the player's co-ordinates where the player was hit
    public void hitHost(Player player)
    {
        String hitGuest = "hit: "+player.getX()+" "+player.getY();
        client.sendTCP(hitGuest);
    }
}
