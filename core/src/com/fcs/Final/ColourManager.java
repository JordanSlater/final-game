/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcs.Final;

import com.badlogic.gdx.graphics.Color;

/**
 *
 * @author Paul
 */
public class ColourManager {
    
    // compares the string from ColourToString and to stringToColour
    // when string is specified colour (eg. "RED" or "BLUE"), will return specified colour
    public static Color stringToColour(String s)
    {
        if(s.equals("RED"))
            return Color.RED;
        if(s.equals("BLUE"))
            return Color.BLUE;
        if(s.equals("YELLOW"))
            return Color.YELLOW;
        if(s.equals("GREEN"))
            return Color.GREEN;
        if(s.equals("ORANGE"))
            return new Color(1, 0.5f, 0, 0);
        if(s.equals("MAGENTA"))
            return Color.MAGENTA;
        if(s.equals("WHITE"))
            return Color.WHITE;
        return Color.BLACK;
    }
    
    // (red, green, blue channels)
    // ColourToSting assigned numbers to modify (red, green, blue) [Like colour picker]
    // converts the floats (colours) to string of a colour
    public static String ColourToString(Color c)
    {
        if(c.r == 1f && c.g == 0f && c.b == 0f)
            return "RED";
        if(c.r == 0f && c.g == 0f && c.b == 1f)
            return "BLUE";
        if(c.r == 1f && c.g == 1f && c.b == 0f)
            return "YELLOW";
        if(c.r == 0f && c.g == 1f && c.b == 0f)
            return "GREEN";
        if(c.r == 1f && c.g == 0.5f && c.b == 0f)
            return "ORANGE";
        if(c.r == 1f && c.g == 0f && c.b == 1f)
            return "MAGENTA";
        if(c.r == 1f && c.g == 1f && c.b == 1f)
            return "WHITE";
        return "BLACK";
    }
}
