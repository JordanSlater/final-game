package com.fcs.Final;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class FinalGame extends Game {
        //game variables
	private GameScreen play; //initialize new GameScreen called play
        private MenuScreen menu; //initialize new MenuScreen called menu
        private Screen currentScreen; //initialize new Screen called currentScreen
        private Music menuMusic; // initialize new Music called menuMusic
        private Music gameMusic; // initialize new Music called gameMusic
        
	@Override
	public void create () {
            
            menu = new MenuScreen(this); //create the MenuScreen that was initialized above
            currentScreen = menu; //set the currentScreen to be menu
            setScreen(play); //set Screen to be play
            
            // create music from core files 
            menuMusic = Gdx.audio.newMusic(Gdx.files.internal("522943_Antiskill--Predator(Menu).mp3"));
            gameMusic = Gdx.audio.newMusic(Gdx.files.internal("591955_Arcane(Background_Track).mp3"));
	}

	@Override
	public void render () {
            if(currentScreen!=null) //if currentScreen has been set
            {
                currentScreen.render(Gdx.graphics.getDeltaTime()); //draw the currentScreen
                if(currentScreen==menu)             // if on menu screen
                {
                    menuMusic.setLooping(true);     // loop the menu theme
//                    menuMusic.play();               // start playing menu theme
                }else
                {
                    menuMusic.stop();               //stop playing menu theme
                    gameMusic.setLooping(true);     //loop the main game theme
//                    gameMusic.play();               //start playing main game theme
                }
            }
	}
    @Override
    public void resize(int width, int height)
    {
        if(play != null) //if play has been set
            play.resize(width, height); //resize play screen to use width and height for its dimensions
        menu.resize(width, height); //resize menu screen to use width and height for its dimensions
    }
    
    public void setMenuScreen()
    {
        currentScreen = menu; //set currentScreen to be menu
        setScreen(menu); //set Screen to be menu
    }
    
    public void setMainGame(boolean isHost, String ip, String playerColour)
    {
        // sends info. to GameScreen class     
        // sends if (if player is host or client, the ip address, and the player's chosen colour)
        play = new GameScreen(this, isHost, ip, playerColour);  
        currentScreen = play; //set currentScreen to be play
        setScreen(play); //set Screen to be play
    }
    
    @Override
    public void dispose() {
        if(play != null) //if play has been set
            play.dispose(); //dispose play
        currentScreen.dispose(); //dispose currentscreen
        menu.dispose(); //dispose menu
        menuMusic.dispose(); //dispose menu theme
        gameMusic.dispose(); //dispose game theme
    }
}
