/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcs.Final;

import com.badlogic.gdx.graphics.Color;

/**
 *
 * @author slatj2095
 */
public class Level {
    //level variables
    private Block[][] blocks; //create an array of an array of blocks for the level
    private Player p1; //create a new Player called p1
    private Player p2; //create a new Player called p2
    
    private float[][] playerCoordinates = new float[2][2];
    
    public static final float GRAVITY = -20; //create constant float for gravity
    public static final int WIDTH = 20; //create constant int for width of blocks
    public static final int HEIGHT = 16; //create constant int for height of blocks
    
    public Level(boolean isHost, String playerColour)
    {
        p1 = new Player(1,1,Color.RED);         // create p1 at [row][col]
        p2 = new Player(1,1,Color.CLEAR);       // create p2 at [row][col]
        // reading text file for map
        // 0s are empty space
        // 1s are blocks
        // 2 is p1's starting position
        // 3 is p2's starting position
        int map[][] = {{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                       {1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1},
                       {1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,1,1},
                       {1,0,3,0,0,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1},
                       {1,1,1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,1},
                       {1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1},
                       {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                       {1,0,0,1,1,1,0,0,0,1,1,0,0,0,1,1,1,0,0,1},
                       {1,0,0,1,1,1,0,0,0,1,1,0,0,0,1,1,1,0,0,1},
                       {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                       {1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1},
                       {1,0,0,2,0,0,0,1,0,0,0,0,1,0,0,0,0,1,1,1},
                       {1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,0,0,0,0,1},
                       {1,1,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,1},
                       {1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,2,0,1},
                       {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}};
        
        blocks = new Block[HEIGHT][WIDTH];
        
        for(int col = 0; col < WIDTH; col++)
        {
            for(int row = (HEIGHT-1); row >= 0;row--)
            {
                if(map[HEIGHT-1-row][col] == 1)
                    blocks[row][col] = new Block();
                if(map[row][col] == 2)
                    if(isHost) {
                        playerCoordinates[0][0] = (col + Block.SIZE/2 - Player.SIZE/2);
                        playerCoordinates[0][1] = ((HEIGHT-1-row) + Block.SIZE/2 - Player.SIZE/2);
                    }
                if(map[row][col] == 3)
                    if(!isHost) {
                        playerCoordinates[1][0] = (col + Block.SIZE/2 - Player.SIZE/2);
                        playerCoordinates[1][1] = ((HEIGHT-1-row) + Block.SIZE/2 - Player.SIZE/2);
                    }
            }
        }
        if(isHost)
            p1 = new Player(respawnPlayer(1,0), respawnPlayer(1,1), ColourManager.stringToColour(playerColour));
        else
            p1 = new Player(respawnPlayer(2,0), respawnPlayer(2,1), ColourManager.stringToColour(playerColour));
    }
    
    public float respawnPlayer(int numPlayer, int XorY)
    {
        return playerCoordinates[numPlayer-1][XorY];
    }
    
    public Block getBlock(int row, int col)
    {
        return blocks[row][col];
    }
    
    public Player getPlayer1() 
    {
        return p1;
    }
    
    public Player getPlayer2() 
    {
        return p2;
    }
}
