/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcs.Final;

import com.badlogic.gdx.Input.TextInputListener;

/**
 *
 * @author front2818
 */
public class IPAddressListener implements TextInputListener {

    /**
     *
     * @param text
     */
    //IP Address variables
    private String text;
    private boolean textInputted;
    
    @Override
    public void input(String text) { //called once OK is clicked
        this.text=text; //sets text string to be what was inputted into text box
        this.textInputted = true; //text is inputted into text box
        if(text.matches("^(\\d){1,3}\\.(\\d){1,3}\\.(\\d){1,3}\\.(\\d){1,3}$")) //check if text matches IP address format
        {
            String[] subText = text.split("\\."); //splits the IP Address into the chunks between periods
            for(String chunk : subText) //for each chunk of the IP Address
            {
                if(Integer.parseInt(chunk) > 255) //check if integer is greater than 255
                {
                    this.textInputted = false; //text is not valid as an IP Address
                }
            }
        }else //if text does not match IP address format
        {
            this.textInputted = false; //text is not valid as an IP Address
        }
    }

    @Override
    public void canceled() {} //cancels text box
    
    public String getText() //retrieve IP address
    {
        return text;
    }
    
    public boolean isTextInputted() //retrieve boolean textInputted
    {
        return textInputted;
    }
}
