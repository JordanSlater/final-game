/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcs.Final;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import java.io.IOException;
import java.util.logging.Logger;


/**
 *
 * @author front2818
 */
public class GameScreen implements Screen {
    private Level level;
    private LevelRenderer renderer;
    private LevelController control;
    private FinalGame game;
    
    //ip address variable
    private String ip;
    
    //server/client variables
    CharServer server;
    CharClient client;
    boolean isHost;
    
    //colour variable
    private String playerColour;
    
    public GameScreen(FinalGame g, boolean isHost, String ip, String playerColour)
    {
        //constructor
        this.game = g;
        this.isHost = isHost;
        this.ip = ip;
        this.playerColour = playerColour;
        create();
    }
	
    public void create () 
    {
        level = new Level(isHost, playerColour); //create the level
        renderer = new LevelRenderer(level); //create the renderer
        //check whether player is the host or the guest
        if(isHost)
        {
            try {
                server = new CharServer(); //create the server
            } catch (IOException ex) {
                Logger.getLogger(FinalGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        } else {
            try {
                client = new CharClient(ip); //create the client with specified IP Address
            } catch (IOException ex) {
                Logger.getLogger(FinalGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        
        control = new LevelController(level, server, client, isHost, playerColour); //create the level controller
        //sets the controller to process any commands
        Gdx.input.setInputProcessor(control);
    }

    @Override
    public void render (float delta) 
    {
            Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1); //define the clearing colour using gray
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); //clear the display buffer to the clear colour
            delta = Math.min(Gdx.graphics.getDeltaTime(), 0.1f); //retrieves change in time between previous frame and current frame
            control.update(delta); //update the controller
            renderer.render(delta); //show the world
    }

    @Override
    public void resize(int width, int height)
    {
        renderer.setSize(width, height); //set the size of renderer to the width and height
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        renderer.dispose(); // dispose renderer
        game.dispose();     // dispose the game
    }
}
