/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcs.Final;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;


/**
 *
 * @author slatj2095
 */
public class MenuScreen implements Screen{
    private FinalGame game;
    //font type
    BitmapFont font = new BitmapFont();
    SpriteBatch batch = new SpriteBatch();
    ShapeRenderer shapes = new ShapeRenderer();
    
    // variable for player's colour (chosen colour sent to final game)
    private Color desiredColour;
    // variable for the spot of where the colour is
    private int colourSpot;
    // specified colours 
    private Color[] colours = {Color.RED, Color.BLUE, Color.YELLOW, Color.GREEN, new Color(1, 0.5f, 0, 0), Color.MAGENTA, Color.WHITE};
    // variable for cycling through colours
    private boolean hasCycled;
    
    IPAddressListener IPAddress = new IPAddressListener();
    
    public static float width;//800
    public static float height;//600
    
    // CONSTRUCTOR (INITIALIZE VARIABLES)
    public MenuScreen(FinalGame g)
    {
        this.game=g;
        desiredColour = colours[0];
    }
    
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1); //define the clearing colour using gray
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); //clear the display buffer to the clear colour
        
        // array of positions for "HOST" button
        int[] buttonHost = new int[4];
        buttonHost[0] = 359;
        buttonHost[1] = 264;
        buttonHost[2] = 85;
        buttonHost[3] = 39;
        
        // array of positions for "JOIN" button
        int[] buttonClient = new int[4];
        buttonClient[0] = 364;
        buttonClient[1] = 164;
        buttonClient[2] = 75;
        buttonClient[3] = 39;
        
        // array of positions for colour button
        int[] buttonColour = new int[4];
        buttonColour[0] = 382;
        buttonColour[1] = 64;
        buttonColour[2] = 39;
        buttonColour[3] = 39;
        
        shapes.begin(ShapeType.Filled);
        shapes.setColor(Color.DARK_GRAY);   // set colour of box
        // draw rectangle, buttonHost, at specified position
        shapes.rect(buttonHost[0],buttonHost[1],buttonHost[2],buttonHost[3]);
        // draw rectnagel, buttonClient, at specified position
        shapes.rect(buttonClient[0],buttonClient[1],buttonClient[2],buttonClient[3]);
        desiredColour = colours[colourSpot];    // sets desiredColour to colour chosen or cycled through
        shapes.setColor(desiredColour);         // sets the colour of the character
        // draw rectangle, buttonColour, at specified position
        shapes.rect(buttonColour[0],buttonColour[1],buttonColour[2],buttonColour[3]);
        shapes.end();
        
        font.setColor(Color.BLACK);         // font colour is black
        font.setScale(3.3f);                // scaling font
        
        batch.begin();
        // draw font of Title, at specified position
        font.draw(batch, "TWITCH FIGHTERS",175,450);      
        font.setScale(2.2f);                // scale font
        font.draw(batch, "Host",365,300);   // draw font for buttonHost
        font.draw(batch, "Join",370,200);   // draw font for buttonClient
        batch.end();
        
        if(Gdx.input.isTouched() 
                && (Gdx.input.getX() > buttonHost[0] && Gdx.input.getX() < buttonHost[0]+buttonHost[2])
                && (Gdx.input.getY() < 600-buttonHost[1] && Gdx.input.getY() > 600-buttonHost[1]-buttonHost[3]))
                //600-buttonHost[1] is due to Gdx.input.getY() measured from top left
        {
            // set main game screen (isHost, ip, and colour)
            // ColourManager.ColourToString calls on the class to manage colour
            game.setMainGame(true, "null", ColourManager.ColourToString(desiredColour));
        }else if(Gdx.input.justTouched()
                && (Gdx.input.getX() > buttonClient[0] && Gdx.input.getX() < buttonClient[0]+buttonClient[2])
                && (Gdx.input.getY() < 600-buttonClient[1] && Gdx.input.getY() > 600-buttonClient[1]-buttonClient[3]))
                //600-buttonClient[1] is due to Gdx.input.getY() measured from top left
        {
            Gdx.input.getTextInput(IPAddress, "IP Address", "Input IP Address of Other Computer");
        }else if(Gdx.input.isTouched()
                && (Gdx.input.getX() > buttonColour[0] && Gdx.input.getX() < buttonColour[0]+buttonColour[2])
                && (Gdx.input.getY() < 600-buttonColour[1] && Gdx.input.getY() > 600-buttonColour[1]-buttonColour[3]))
                //600-buttonClient[1] is due to Gdx.input.getY() measured from top left
        {
            // when player has touched the colour chooser, goes thru this if statment
            // if hasCycled is false
            if(hasCycled == false)
            {               
                colourSpot++;               // cycle through the arrays of colour
                if(colourSpot == 7)         // when at the end of arrays of colour (spot where player's colour is white)
                    colourSpot = 0;         // reset, for cycling through colours again
                hasCycled = true;           // set hasCycled to true
            }
        } else {                            // if hasCycled is not true
            hasCycled = false;              // set hasCycled to false
        }
        // calls the IPAddressListener (method returns boolean)
        if(IPAddress.isTextInputted())
        {
            // sends if (if the player is a client or host, sends string of IP, and the player's chosen colour)
            game.setMainGame(false, IPAddress.getText(), ColourManager.ColourToString(desiredColour));
        }
        
        // if player presses 'ESCAPE' 
        if(Gdx.input.isKeyPressed(Keys.ESCAPE)) {
            Gdx.app.exit();     // exit out of application
        }
        
    }
    
    @Override
    public void show() {
        
    }

    @Override
    public void hide() {
        
    }

    @Override
    public void pause() {
        
    }

    @Override
    public void resume() {
        
    }

    @Override
    public void dispose() {
        font.dispose(); //dispose of font
        batch.dispose(); //dispose of batch
        shapes.dispose(); //dispose of shapes
    }

    @Override
    public void resize(int width, int height) {
        this.width = width; //resize width
        this.height = height; //resize height
    }
    
}
