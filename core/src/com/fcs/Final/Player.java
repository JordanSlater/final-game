/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcs.Final;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.graphics.Color;
import java.awt.Graphics;

/**
 *
 * @author slatj2095
 */
public class Player {
    
    // create constant for wall jump delay
    private final static long WALL_TIME = 200;
    
    // create constant for player's size (0.7)
    public static final float SIZE = 0.7f;
    // create constant for player's max speed
    public static final float ACCEL = 10f;
    // create constant for the player when jumping
    public static final float JUMP_VEL = 3f;
    // create constant for max acceleration of player
    private static final float ACCELERATION = 30;
    // create constant for max jump speed of player
    private static final float MAX_JUMP_SPEED = 7;
    // create constant for dampening
    private static final float DAMP = 0.95f;
    // create constant of max velocity limit 
    public static final float MAX_VELOCITY = 7;
    
    //shield variables
    public static final float LENGTH_SHIELD = Player.SIZE+0.4f;
    public static final float WIDTH_SHIELD = 0.2f;
    public static final float RADIUS_SHIELD = (float)(Player.SIZE/Math.sqrt(2));
    
    // STATE OF PLAYER
    public enum State
    {
        ONGROUND, ONLEFTWALL, ONRIGHTWALL, JUMPING, DYING
    }
    
    // ACTIONS OF PLAYER
    public enum Action
    {
        NOACTION, ATTACK, SHIELD
    }
    
    // DIRECTION CHOSEN BY PLAYER
    public enum Direction
    {
        N,NE,E,SE,S,SW,W,NW,STILL
    }
    
   
    private Rectangle bounds;           // RECTANGLE VARIABLES
    private Vector2 Pos,Vel,Accel;      // VECTOR VARIABLES
    
    // ENUM VARIABLES
    private State state;
    private Action action;
    private Direction direction;
    
    private double stamina;             // STAMINA
    private double rate=0.1;            // STAMINA RATE
    private Color colour;
    private int jumpDirection = 0;      // 
    private float deathTick = 1;        // used for respawn death animation
    
    private float deathX;               // position of death-x
    private float deathY;               // position of death-y
    
    private Sound dyingSound;
    
    // CONSTRUCTOR (INTIALIZE VARIABLES)
    // (x-position, y-postion, stamina amount, player colour)    
    public Player(float x, float y, Color colour)   
    {
        Pos = new Vector2(x,y);
        bounds = new Rectangle(x, y, SIZE, SIZE);
        Vel = new Vector2(0,0);
        Accel = new Vector2(0,0);
        state = State.ONGROUND;
        this.stamina = 100;
        action= Action.NOACTION;
        direction=Direction.STILL;
        this.colour = colour;
        deathTick = 1;
        dyingSound = Gdx.audio.newSound(Gdx.files.internal("Block.wav"));
    }
    
    // UPDATING PLAYER'S VECTOR ADDITION
    public void update(float delta)
    {
        // if player's action is not attacking
        if(action != Player.Action.ATTACK)
            Accel.y = Level.GRAVITY;       // gravity pulls player down       
        
        Accel.scl(delta);                   // acceleration is added to velocity
        Vel.add(Accel);                     // add velocity to acceleration
        
        // if player's accel.x is zero
        if(Accel.x == 0)
            Vel.x *= DAMP;                  // slow down the player (dampening)
        if(Vel.x > MAX_VELOCITY)            // if player's vel.x is over MAX_VELOCITY        
            Vel.x = MAX_VELOCITY;           // player reaches and stays at constant speed (going right)
        else if(Vel.x < -MAX_VELOCITY)      // if player's. vel.x is below -MAX_VELOCITY
            Vel.x = -MAX_VELOCITY;          // player reaches and stays at constant speed (going left)
        
        if(state == State.DYING)            // if player's state is dying
            deathTick-=0.02f;               // keep substracting deathTick
        if(deathTick < 0)                   // if deathTick is less than 0
            deathTick = 0;                  // set a limit to deathTick
      
        // adds the velocity to the player's position
        Pos.add(Vel.cpy().scl(delta));
        
        // if player's state is DYING
        if(state == Player.State.DYING)
        {
            Pos.x = deathX;                 // get x-position of death
            Pos.y = deathY;                 // get y-position of death
        }
    }
    
    // ABILITY FOR PLAYER TO JUMP
    public void jump()
    {
        Vel.y = MAX_JUMP_SPEED;             // set player's vel.y to max jump speed
        if(state == State.ONLEFTWALL)       // when player is on left wall, 
            Vel.x = MAX_VELOCITY/2;         // vel.x is divided by 2
        if(state == State.ONRIGHTWALL)      // when player is on right wall, 
            Vel.x = -MAX_VELOCITY/2;        // vel.x is divided by 2
    }
    
    // ABILITY FOR PLAYER TO REGENERATE STAMINA
    public void regenerate() 
    {
        rate=0.4;                           // rate of stamina regeneration
        if(stamina >= 0 || stamina <= 99)   // if player stamina is between 0 and 99, then
            stamina+=rate;                  // regenerating stamina 
        if(stamina > 100)                   // if player stamina is 100 or over, then
            stamina = 100;                  // prevents stamina from going above 100
        if(stamina < 0)                              
            stamina = 0;                    // prevents stamina from going below 0
    }
    
    // PLAYER DRAINS STAMINA WHEN USING SHIELD
    public void shieldDrain()
    {
        if(stamina >= 0 || stamina <= 99)   // if player stamina is between 0 and 99, then
        {
            stamina-=rate;                  // drain stamina
            rate+=0.005;                    // exponential increasing shield drain
        } 
        if(stamina > 100)                   // if player stamina is 100 or over, then        
            stamina = 100;                  // limit stamina        
        if(stamina > 100)                   // if player stamina is 100 or over, then       
            stamina = 100;                  // set stamina limit to 100        
        if(stamina < 0)                     // if player stamina is below 0, then
            stamina = 0;                    // set stamina limit to 0
    }
    
    // GETTERS (
    
    public float getX()
    {
        return Pos.x;
    }
    
    public float getY()
    {
        return Pos.y;
    }
     
    public float getXVel() 
    {
        return Vel.x;
    }
    
    public float getYVel() 
    {
        return Vel.y;
    }
    
    public float getXAccel() {
        return Accel.x;
    }
    
    public float getYAccel() 
    {
        return Accel.y;
    }
    
    public int getJumpDirection()
    {
        return jumpDirection;
    }
        
    public State getState()
    {
        return state;
    }
    
    public Direction getDirection()
    {
        return direction;
    }
    
    public double getStamina() 
    {
        return stamina;
    }
    
    public Action getAction() 
    {
        return action;
    }
    
    public Color getColor()
    {
        return colour;
    }
    
    public float getDeathTick()
    {
        return deathTick;
    }
    
    // ) GETTERS
       
    // SETTERS (
    
    public void setX(float x)
    {
        Pos.x = x;
    }
    
    public void setY(float y)
    {
        Pos.y = y;
    }
    
    public void setXVel(float v)
    {
        Vel.x = v;
    }
    
    public void setYVel(float v)
    {
        Vel.y = v;
    }
    
    public void setXAccel(float a)
    {
        Accel.x = a;
    }
    
    public void setYAccel(float a)
    {
        Accel.y = a;
    }
    
    public void setJumpDirection(int i)
    {
        jumpDirection = i;
    }
    
    public void setState(State s)
    {
        state = s;
    }
    
    public void setDirection(Direction d)
    {
        direction = d;
    }
    
    public void setStamina(double s) 
    {
        stamina = s;
    }
    
    public void setAction(Action a) 
    {
        action = a;
    }
    
    public void setColour(Color c)
    {
        colour = c;
    }
    
    public void setDeathTick(float f)
    {
        deathTick = f;
    }
    
    // SETTERS )
    
    public void kill()
    {
        state = Player.State.DYING;
        deathX = Pos.x;
        deathY = Pos.y;
//        dyingSound.play();
        action = Action.NOACTION;
    }
    
    public void respawn(float x, float y)
    {
        Pos.x = x;
        Pos.y = y;
        state = State.JUMPING;
        action = Action.NOACTION;
        stamina = 100;
        deathTick = 1;
    }
    
    public Direction stringToDirection(String s) {
        return Player.Direction.valueOf(s);
    }
    
    public State stringToState(String s) {
        return Player.State.valueOf(s);
    }
    
    public Action stringToAction(String s) {
        return Player.Action.valueOf(s);
    }
    
    public float getDegrees() {
        if(direction == Player.Direction.N)
            return 0f;
        if(direction == Player.Direction.NW)
            return 45f;
        if(direction == Player.Direction.W)
            return 90f;
        if(direction == Player.Direction.SW)
            return 135f;
        if(direction == Player.Direction.S)
            return 180f;
        if(direction == Player.Direction.SE)
            return 225f;
        if(direction == Player.Direction.E)
            return 270f;
        if(direction == Player.Direction.NE)
            return 315f;
        return 0f;
    }
}
