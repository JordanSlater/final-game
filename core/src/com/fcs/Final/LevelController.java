 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcs.Final;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author chea0415
 */
public class LevelController implements InputProcessor{

    // CONSTANTS FOR JUMP DELAY, SHIELD WAIT TIME, AND DELAY TIME
    private final static long JUMP_PRESS_TIME = 150;
    private final static long SHIELD_WAIT_TIME = 1500;
    private final static long SHIELD_DELAY_TIME = 1000;
    // CONSTANT FOR WALL JUMPING
    private final static long WALL_TIME = 200;
    
    // player and level variables
    private Player p1;
    private Player p2;
    private Level level;
    
    private CharClient client;
    private CharServer server;
    
    // Player control variables
    private boolean right = false;
    private boolean left = false;
    private boolean jump = false;
    private boolean attack = false;
    private boolean shield = false;
    private boolean up = false;
    private boolean down = false;
    private boolean exit = false;
    private boolean shieldOn = true;
    private boolean delayShield = false;
    private boolean shieldTemp;
    
    //Host/Client boolean
    public boolean isHost;
    
    //time variables
    private double healthTime;
    private double delayTime;
    private long jumpPress;
    private long wallTime;
    
    //sounds variables
    private Sound shieldSound;
    private Sound explosion;
    private Sound block;
    
    // CONSTRUCTOR (INITIALIZE VARIABLES)
    public LevelController(Level level, CharServer server, CharClient client, boolean isHost, String playerColour) 
    {
        this.level = level;
        p1 = level.getPlayer1();
        p2 = level.getPlayer2();
        this.isHost = isHost;
        if(this.isHost)
        {
            this.server = server;
        } else {
            this.client = client;
            this.client.initialize(p1);
        }
        shieldSound = Gdx.audio.newSound(Gdx.files.internal("Shield.wav")); // upload shield sound
        explosion = Gdx.audio.newSound(Gdx.files.internal("Explosion.mp3")); // upload explosion sound
        block = Gdx.audio.newSound(Gdx.files.internal("Block.wav"));    // upload blocking sound
    }
    
    private void processInput() 
    {
        if(p1.getAction() != Player.Action.ATTACK)          // if player is not attacking
        {
            p1.setDirection(Player.Direction.STILL);        // direction is still
            if(left)                                        // if player goes left, then
            {
                if(p1.getAction() != Player.Action.SHIELD)  // if player is not shielding
                    p1.setXAccel(-Player.ACCEL);            // player accels to the left
                p1.setDirection(Player.Direction.W);        // direction set to W
                if(up)                                      // if player presses left and up
                    p1.setDirection(Player.Direction.NW);   // direction set to NW
                if(down)                                    // if player presses left and down
                    p1.setDirection(Player.Direction.SW);   // direction set to SW
            }
            
            if(right)                                       // if player presses right, then
            {
                if(p1.getAction() != Player.Action.SHIELD)  // if player is not shielding
                    p1.setXAccel(+Player.ACCEL);            // player accels to the right
                p1.setDirection(Player.Direction.E);        // direction set to E (if pressing E)
                if(up)                                      // if player presses right and up
                    p1.setDirection(Player.Direction.NE);   // direction set to NE
                if(down)                                    // if player presses right and down
                    p1.setDirection(Player.Direction.SE);   // direction set to SE
            }

            if(!left && !right || left && right)            // if player both pressing left and right
            {
                if(p1.getAction() != Player.Action.SHIELD)  // and player is not shielding
                    p1.setXAccel(0);                        // no accel added
                if(up)                                      // if left && right and up
                    p1.setDirection(Player.Direction.N);    // set direction N
                if(down)                                    // if left && right and down
                    p1.setDirection(Player.Direction.S);    // set direction S
            }
        }
        
        if(p1.getAction() != Player.Action.ATTACK)
            checkCollisions();  // method to check collisions
        // if player presses 'SPACE' (jump button)
        if(jump) 
        {
            // state of player is not jumping and action is not shielding
            if(p1.getState() != Player.State.JUMPING && p1.getAction() != Player.Action.SHIELD) {
                p1.jump();                                  // player jumps
                jumpPress = System.currentTimeMillis();     // takes time at which player has jumped
                p1.setState(Player.State.JUMPING);          // state is set to jumping
            }
            else {
                // when the time limit is reached
                if(System.currentTimeMillis() - jumpPress < JUMP_PRESS_TIME)
                    p1.jump();          // player can jump again
            }
        }
        // if player presses 'O' (shield button)
        if(shield)
        {
            // if shield is pressed, and action is not shielding
            if(p1.getAction()!=Player.Action.SHIELD)      
            {
                p1.setAction(Player.Action.SHIELD);     // set action to 'SHIELD'
                shieldSound.play(0.5f);                     // play shielding sound
            }   
        }else                                           // else if player is not shielding
        {
            p1.setAction(Player.Action.NOACTION);       // no action is set
        }
        
        // when the shield is true, player's stamina not 0 and player's action is not attacking, then
        if(shield && p1.getStamina()!=0 && p1.getAction() != Player.Action.ATTACK)
        {
            p1.shieldDrain();                           // drain the shield
            healthTime = System.currentTimeMillis();    // take time at which if statment true
            if(p1.getStamina()==0)                      // when stamina is 0
            {   
                shieldOn = false;                       // shieldOn is off
                shield = false;                         // shield is off
            }
        }else {                             
            p1.regenerate();                            // regenerate stamina
            // if the time limit has been over the SHIELD_WAIT_TIME
            if(System.currentTimeMillis()-healthTime >= SHIELD_WAIT_TIME)
            {
                shieldOn=true;                          // player can shield now
            }
        }
        
        // a temporary variable used to keep track of if shield is true
        if(shieldTemp == true) 
        {
            delayShield = shieldTemp;                   // delayShield (false) is now (true)
            delayTime = System.currentTimeMillis();     // get time of when player is shielding           
        } else {                                        // temp variable is false, shield is false
            if(System.currentTimeMillis() - delayTime >= SHIELD_DELAY_TIME) 
            {
                delayShield = false;    // player can shield now
            }
        }
        
        // when the shield is true, player's stamina not 0 and player's action is not attacking, then
        if(shield && p1.getStamina()!=0 && p1.getAction() != Player.Action.ATTACK)
        {
            p1.shieldDrain();                           // drain the shield
            healthTime = System.currentTimeMillis();    // take time at which if statment true
            if(p1.getStamina()==0)                      // when stamina is 0
            {   
                shieldOn = false;                       // shieldOn is off
                shield = false;                         // shield is off
            }
        }else {                             
            p1.regenerate();                            // regenerate stamina
            // if the time limit has been over the SHIELD_WAIT_TIME
            if(System.currentTimeMillis()-healthTime >= SHIELD_WAIT_TIME)
            {
                shieldOn=true;                          // player can shield now
            }
        }
        
        // a temporary variable used to keep track of if shield is true
        if(shieldTemp == true) 
        {
            delayShield = shieldTemp;                   // delayShield (false) is now (true)
            delayTime = System.currentTimeMillis();     // get time of when player is shielding           
        } else {                                        // temp variable is false, shield is false
            if(System.currentTimeMillis() - delayTime >= SHIELD_DELAY_TIME) 
            {
                delayShield = false;    // player can shield now
            }
        }               
        
        // if player presses 'O'(attack button)  and chooses direction 
        if(attack)
        {
            // if attacking and the player has chosen a direction, then
            if(p1.getDirection() != Player.Direction.STILL)
                p1.setAction(Player.Action.ATTACK);         // set player action 'ATTACK'
            if(p1.getAction() == Player.Action.ATTACK)      // if action is 'ATTACK'
            {
                if(p1.getDirection() != Player.Direction.STILL) // and direction is not 'STILL'
                {
                    // set the X,Y Accel and Vel to 0
                    p1.setXAccel(0);                            
                    p1.setYAccel(0);
                    p1.setXVel(0);
                    p1.setYVel(0);
                }
            }
            float speed = 2f; // the distance travelled per frame while attacking (2f)
            double component = speed*(Math.sqrt(2)/2); //variable for the component of a diagonal attack
            // if the player is going up add the speed to the player's y value
            if(p1.getDirection() == Player.Direction.N)
                attackCollisions(0,speed);
            // the same applies to the other horizontal and vertical directions based on thier bearing
            if(p1.getDirection() == Player.Direction.S)
                attackCollisions(0,-speed);
            if(p1.getDirection() == Player.Direction.E)
                attackCollisions(speed,0);
            if(p1.getDirection() == Player.Direction.W)
                attackCollisions(-speed,0);
            // is the player is going up and right add the diagonal component in both the x value, and the y value
            if(p1.getDirection() == Player.Direction.NE)
                attackCollisions((float)(component),(float)(component));
            // the same applies to the other diagonal directions based on their bearing
            if(p1.getDirection() == Player.Direction.NW)
                attackCollisions((float)(-component),(float)(component));
            if(p1.getDirection() == Player.Direction.SE)
                attackCollisions((float)(component),(float)(-component));
            if(p1.getDirection() == Player.Direction.SW)
                attackCollisions((float)(-component),(float)(-component));
        }
    }
    
    public void attackCollisions(float addX, float addY)
    {
        // where the player is: 
        float x = p1.getX();
        float y = p1.getY();
        // where the player is going to be: 
        float newX = x + addX;
        float newY = y + addY;
        
        // if the player's co-ordinates are outside the level, make them inside the level
        if(x < 0)
            x = 0;
        if(y < 0)
            y = 0;
        if(x > Level.WIDTH)
            x = Level.WIDTH - 1;
        if(y > Level.HEIGHT)
            y = Level.HEIGHT - 1;
        
        // if where the player is going to be is outside the level, make the co-ordinates inside the level
        if(newX < 0)
            newX = 0;
        if(newY < 0)
            newY = 0;
        if(newX > Level.WIDTH)
            newX = Level.WIDTH - 1;
        if(newY > Level.HEIGHT)
            newY = Level.HEIGHT - 1;
        
        // extra is so that if the player is up against a wall it does not "hit the wall and glitch into the position"
        float extra = 0.001f;
        float diaMargin = (float)(Math.sqrt(Math.pow(Block.SIZE/2, 2)+Math.pow(Block.SIZE/2, 2))+Player.SIZE/2);
        
        // Horizontal and Vertical directional directions: 
        // the direction the player moves in: 
        if(p1.getDirection() == Player.Direction.N){
            // for loops: checking for blocks to collide with
            
            // checking for blocks that are perpendicular to direction of attack
            // adding extra for when the player is touching the right side of the wall
            // subtracting extra for when the player is touching the left side of the wall
            //i++ because the player is progressing up (i-- if player was progressing down)
            for(int i = (int)(x + extra); i <= (int)(newX + Player.SIZE - extra); i++)
            {
                // checking for blocks that are in the direction of attack
                for (int j = (int)(y + Player.SIZE/2); j <= (int)(newY + Player.SIZE/2); j++)
                {
                    // if there is a block in that spot
                    if(level.getBlock(j, i) != null)
                    {
                        // place player below the block that it collided with
                        newY = j-Player.SIZE;
                        // stop the attack button from being pressed
                        attack = false;
                        // stop the player from attacking
                        p1.setAction(Player.Action.NOACTION);
                    }
                }
            }
            
        }
        //if attacking direction is south
        if(p1.getDirection() == Player.Direction.S){
            for(int i = (int)(x + extra); i <= (int)(newX + Player.SIZE - extra); i++)
            {
                for (int j = (int)(y + Player.SIZE/2); j >= (int)(newY + Player.SIZE/2); j--)
                {
                    if(level.getBlock(j, i) != null)
                    {
                        newY = j+Block.SIZE;
                        attack = false;
                        p1.setAction(Player.Action.NOACTION);
                    }
                }
            }
        }
        //if attacking direction is east
        if(p1.getDirection() == Player.Direction.E){
            for(int i = (int)(x + Player.SIZE/2); i <= (int)(newX + Player.SIZE/2); i++)
            {
                for (int j = (int)(y + extra); j <= (int)(newY + Player.SIZE - extra); j++)
                {
                    if(level.getBlock(j, i) != null)
                    {
                        newX = i-Player.SIZE;
                        attack = false;
                        p1.setAction(Player.Action.NOACTION);
                    }
                }
            }
        }
        //if attacking direction is west
        if(p1.getDirection() == Player.Direction.W){
            for(int i = (int)(x + Player.SIZE/2); i >= (int)(newX + Player.SIZE/2); i--)
            {
                for (int j = (int)(y + extra); j <= (int)(newY + Player.SIZE - extra); j++)
                {
                    if(level.getBlock(j, i) != null)
                    {
                        newX = i+Block.SIZE;
                        attack = false;
                        p1.setAction(Player.Action.NOACTION);
                    }
                }
            }
        }
        // end Horizontal and Vertical directional directions
        
        // Attacking in the diagonal directions:
        // if the attacking direction is north east
        if(p1.getDirection() == Player.Direction.NE){
            for(int i = (int)(x + Player.SIZE/2); i <= (int)(newX + Player.SIZE/2); i++)
            {
                for (int j = (int)(y + Player.SIZE/2); j <= (int)(newY + Player.SIZE/2); j++)
                {
                    if(level.getBlock(j, i) != null) 
                    {
                        float bBlock = (i + Block.SIZE/2) + (j + Block.SIZE/2);
                        float bPlayer = (y + Player.SIZE/2) - (x + Player.SIZE/2);
                        float poiX = (bBlock - bPlayer)/2.0f;
                        float poiY = poiX + bPlayer;
                        float dBlock = distance(poiX,poiY,(i+Block.SIZE/2),(j+Block.SIZE/2));
                        if(dBlock < diaMargin)
                        {
                            if((i+Block.SIZE/2) < poiX) {
                                // colliding with the bottom of the block
                                newY = (j-Player.SIZE);
                                newX = newY+Player.SIZE/2-bPlayer;
                                attack = false;
                                p1.setAction(Player.Action.NOACTION);
                                p1.setXVel(Player.MAX_VELOCITY);
                            } else {
                                // colliding with the left of the block
                                newX = (i-Player.SIZE);
                                newY = newX+Player.SIZE/2+bPlayer;
                                attack = false;
                                p1.setAction(Player.Action.NOACTION);
                                p1.setYVel(Player.MAX_VELOCITY);
                            }
                        }
                    }
                }
            }
        }
        // if the attacking direction is north west
        if(p1.getDirection() == Player.Direction.NW){
            for(int i = (int)(x + Player.SIZE/2); i >= (int)(newX + Player.SIZE/2); i--)
            {
                for (int j = (int)(y + Player.SIZE/2); j <= (int)(newY + Player.SIZE/2); j++)
                {
                    if(level.getBlock(j, i) != null)
                    {
                        float bBlock = (j+Block.SIZE/2)-(i+Block.SIZE/2);
                        float bPlayer = (y + Player.SIZE/2) + (x + Player.SIZE/2);
                        float poiX = (bPlayer - bBlock)/2.0f;
                        float poiY = -poiX + bPlayer;
                        float dBlock = distance(poiX,poiY,(i+Block.SIZE/2),(j+Block.SIZE/2));
                        if(dBlock < diaMargin)
                        {
                            if((i+Block.SIZE/2) < poiX)
                            {
                                // colliding with the right of the block
                                newX = (i+Block.SIZE);
                                newY = -(newX+Player.SIZE/2)+bPlayer;
                                attack = false;
                                p1.setAction(Player.Action.NOACTION);
                                p1.setYVel(Player.MAX_VELOCITY);
                            } else {
                                // colliding with the bottom of the block
                                newY = (j-Player.SIZE);
                                newX = -((newY+Player.SIZE/2)-bPlayer)-Player.SIZE/2;
                                attack = false;
                                p1.setAction(Player.Action.NOACTION);
                                p1.setXVel(-Player.MAX_VELOCITY);
                            }
                        }
                    }
                }
            }
        }
        // if the attacking direction is south east
        if(p1.getDirection() == Player.Direction.SE){
            for(int i = (int)(x + Player.SIZE/2); i <= (int)(newX + Player.SIZE/2); i++)
            {
                for (int j = (int)(y + Player.SIZE/2); j >= (int)(newY + Player.SIZE/2); j--)
                {
                    if(level.getBlock(j, i) != null)
                    {
                        float bBlock = (j+Block.SIZE/2)-(i+Block.SIZE/2);
                        float bPlayer = (y + Player.SIZE/2) + (x + Player.SIZE/2);
                        float poiX = (bPlayer - bBlock)/2.0f;
                        float poiY = -poiX + bPlayer;
                        float dBlock = distance(poiX,poiY,(i+Block.SIZE/2),(j+Block.SIZE/2));
                        if(dBlock < diaMargin)
                        {
                            if((i+Block.SIZE/2) < poiX)
                            {
                                // colliding with the top of the block
                                newY = (j+Block.SIZE);
                                newX = -(newY+Player.SIZE/2)+bPlayer;
                                attack = false;
                                p1.setAction(Player.Action.NOACTION);
                                p1.setXVel(Player.MAX_VELOCITY);
                            } else {
                                // colliding with the left of the block
                                newX = (i-Player.SIZE);
                                newY = -(newX+Player.SIZE/2)+bPlayer-Player.SIZE/2;
                                attack = false;
                                p1.setAction(Player.Action.NOACTION);
                                p1.setYVel(-Player.MAX_VELOCITY);
                            }
                        }
                    }
                }
            }
        }
        // if the attacking direction is south west
        if(p1.getDirection() == Player.Direction.SW){
            for(int i = (int)(x + Player.SIZE/2); i >= (int)(newX + Player.SIZE/2); i--)
            {
                for (int j = (int)(y + Player.SIZE/2); j >= (int)(newY + Player.SIZE/2); j--)
                {
                    if(level.getBlock(j, i) != null)
                    {
                        float bBlock = (i+Block.SIZE/2)+(j+Block.SIZE/2);
                        float bPlayer = (y + Player.SIZE/2) - (x + Player.SIZE/2);
                        float poiX = (bBlock - bPlayer)/2.0f;
                        float poiY = poiX + bPlayer;
                        float dBlock = distance(poiX,poiY,(i+Block.SIZE/2),(j+Block.SIZE/2));
                        if(dBlock < diaMargin)
                        {
                            if((i+Block.SIZE/2) < poiX)
                            {
                                // colliding with the right of the block
                                newX = (i+Block.SIZE);
                                newY = (newX+Player.SIZE/2)+bPlayer-Player.SIZE/2;
                                attack = false;
                                p1.setAction(Player.Action.NOACTION);
                                p1.setYVel(-Player.MAX_VELOCITY);
                            } else {
                                // colliding with the top of the block
                                newY = (j+Block.SIZE);
                                newX = (newY+Player.SIZE/2)-bPlayer-Player.SIZE/2;
                                attack = false;
                                p1.setAction(Player.Action.NOACTION);
                                p1.setXVel(-Player.MAX_VELOCITY);
                            }
                        }
                    }
                }
            }
        }
        boolean blocked = false;
        if(p2.getAction() == Player.Action.SHIELD)
            if(Math.sin(p1.getDegrees()) - Math.sin(p2.getDegrees()) != 0)
                blocked = true;
        
        float p2Distance = 10*Player.SIZE;
        
        
        // this is the distance from the centre of the player to the attacking point(the vertice that hits the player)
        // takes the base of the triangle and multiplies it by sin 60 (gives the hieght of the triangle)
        // then subtracts half of the base and multyplies it by tan 30 (gives the hieght to the centre of the player)
        float verticalUp = (float)(Player.SIZE*Math.sin(Math.toRadians(60)) - (Player.SIZE/2*Math.tan(Math.toRadians(30))));
        // finds the distance from the current co-ordinates of the player to the new co-ordinates
        float attackLimit = distance(p1.getX(), p1.getY(), newX, newY);
        // finds co-ordinates for the attacking vertices using sin and cos, and multipling them by the player's direction of attack
        // 360-player's degrees means to rotate in the other direction as libgdx
        float attackStartX = p1.getX() + Player.SIZE/2 + verticalUp*(float)(Math.sin(Math.toRadians(360-p1.getDegrees())));
        float attackStartY = p1.getY() + Player.SIZE/2 + verticalUp*(float)(Math.cos(Math.toRadians(360-p1.getDegrees())));
        // finds co-ordinates for the end of the attack vector using sin and cos, and multipling them by the player's direction of attack
        float attackEndX = attackStartX + attackLimit*(float)(Math.sin(Math.toRadians(360-p1.getDegrees())));
        float attackEndY = attackStartY + attackLimit*(float)(Math.cos(Math.toRadians(360-p1.getDegrees())));
        // the vector to represent the area of attack using the difference in start and end of the attack (as calculated above)
        Vector2 attack = new Vector2(attackEndX - attackStartX, attackEndY - attackStartY);
        // creates a vector from the starting point of attack to the opposing player
        Vector2 toVictim = new Vector2((p2.getX() + Player.SIZE/2) - attackStartX, (p2.getY() + Player.SIZE/2) - attackStartY);
        // finds the angle between the two vectors
        float angle = attack.angle(toVictim);
        // creates a projection of the second player's vector on to the attacking vector using projection math
        // reference: http://math.oregonstate.edu/home/programs/undergrad/CalculusQuestStudyGuides/vcalc/dotprod/dotprod.html
        Vector2 projection = (attack.cpy().scl((attack.dot(toVictim)) / (float)(Math.pow(attack.len(), 2))));
        // if the opposing player's projection is less than the attacking vector
        if(projection.len() < attack.len())
        {
            // if the opposing player is shielding the opposite direction of the attack and 
            // if the player is withing the shielding range 
            // (using the angle between the vectors taking the sin of it and multiplying by the length of the opposing player's vector)
            // this gets the perpendicular length to the attacking vector
            // if this value is less that half of the length of the shield the attacking player is blocked
            if(blocked && (float)(Math.abs(toVictim.len() * Math.sin(Math.abs(angle)))) < Player.LENGTH_SHIELD/2)
            {
                // subtracts the player's shield radius and width from the projection vector
                // this subtraction works because the value is then divided by the length of the vector
                projection.scl((projection.len() - (Player.RADIUS_SHIELD + Player.WIDTH_SHIELD))/projection.len());
                // sets the new position to be where the projection vector ends
                newX = ((p1.getX() + Player.SIZE/2) + projection.x) - Player.SIZE/2;
                newY = ((p1.getY() + Player.SIZE/2) + projection.y) - Player.SIZE/2;
                // stops the player from attacking
                p1.setAction(Player.Action.NOACTION);
                // play the blocking sound
                block.play(0.5f);
            // if the opposing player has not blocked the attack
            } else
            // if the opposing player is not behind the attacking player
            // (remember angle is the angle between the attack and the opposing player)
            if(Math.abs(angle) < 90)
                // the perpendicular distance between the attacking vector and the opposing player
                p2Distance = (float)(Math.abs(toVictim.len()*Math.sin(Math.abs(angle))));

        }
        
        // variable used for the distance from the centre of the player to its corner or edge
        float stateDistance = 0f;
        
        // if the second player is jumping (ie. is a circle) the distance used for calculations is the circle's radius (half of the player's size)
        if(p2.getState() == Player.State.JUMPING) {
            stateDistance = Player.SIZE/2;
        } else {
            // if the player is not jumping (ie: is a square)
            // the distance to the edge of the attacked player changes relative to the player's attack direction
            // if the player is not moving diagonally the distance is to the edge
            // if the player is moving diagonally the distance is to the corner
            if(p1.getDirection() == Player.Direction.N || p1.getDirection() == Player.Direction.E
            || p1.getDirection() == Player.Direction.S || p1.getDirection() == Player.Direction.W) {
                stateDistance = Player.SIZE/2;
            } else {
                // finding the total diagonal distance from one corner to the other using pythagorean theorem then dividing by 2
                stateDistance = (float)((Math.sqrt(Math.pow(Player.SIZE, 2)+Math.pow(Player.SIZE, 2)))/2);
            }
        }
        // if the player's perpendicular distance is with the size of the player the player has been hit
        if(p2Distance < stateDistance)
        {
            // if the host player is attacking the guest
            if(isHost)
            {
                // kill the guest(all logic should be done by the host)
                server.hitGuest(p2);
                p2.kill();
                explosion.play(0.5f);   // player explosion sound when host dies
            } else {
                // send a hit request with the attacking player's co-ordinates and where it will be
                client.hitHost(p2);
                p2.kill();
                explosion.play(0.5f);   // player explosion sound when client dies
            }
        }
        // move the player to the where it should be at the end of the attack
        p1.setX(newX);
        p1.setY(newY);
    }
    
    private float distance(float x1, float y1, float x2, float y2)
    {
        // finds the distance between to points using pythagorean theorem
        return (float)(Math.sqrt(Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2)));
    }
    
    public float normalize(float angle)
    {
        if(angle >= 360)
            angle -= 360;
        if(angle < 0)
            angle += 360;
        return angle;
    }
    
    private void checkCollisions() 
    {
        // SAT (seperate axis theory)
        // make variables for checking the x and y of the player's left bottom corner
        int x = (int)(p1.getX());
        int y = (int)(p1.getY());
        // make variables for checking the x and y of the player's right top corner
        int endX = (int)(p1.getX() + Player.SIZE);
        int endY = (int)(p1.getY() + Player.SIZE);
        // used for caculating overlap
        float overX = 0;
        float overY = 0;
        
        if(System.currentTimeMillis() - wallTime > WALL_TIME)
            p1.setState(Player.State.JUMPING);
        // used for making the player slide over the cracks in the blocks
        boolean slideX = false;
        
        // if the blocks are oriented like this: 
        // 1 = block, 0 = not a block
        // 00
        // 11
        if(level.getBlock(y, x) != null && level.getBlock(y, endX) != null
        && level.getBlock(endY, x) == null && level.getBlock(endY, endX) == null)
        {
            // slide over the crack between the blocks
            slideX = true;
        }
        
        // goes though the block surrounding the player
        for(int i = x; i <= endX; i++)
        {
            for (int j = y; j <= endY; j++)
            {
                // if there is a block at the current position
                if(level.getBlock(j, i) != null)
                {
                    // hit something so calaulate overlap
                    if(x < i) // collision on the right
                    {
                        overX = (p1.getX()) + Player.SIZE - i;
                    } else { // left collision
                        overX = i + Block.SIZE - (p1.getX());
                    }

                    if(y < j) // collision on the top
                    {
                        overY = (p1.getY()) + Player.SIZE - j;
                    } else { // collision on the bottom
                        overY = j + Block.SIZE - (p1.getY());
                    }
                    // if the player is farther into the block vertically
                    if(overY < overX)
                    {
                        // stop the player from going farther into the block vertically
                        p1.setYVel(0);
                        // if the player is colliding with the top of the block
                        if(y < j)
                        {
                            // move the player out of the block vertically down
                            p1.setY(p1.getY() - overY);
                        // if the player is colliding with the top of the block
                        }else {
                            // move the player out of the block vertically up
                            p1.setY(p1.getY() + overY);
                            // set the player's state to be on the ground
                            p1.setState(Player.State.ONGROUND);
                        }
                    // if the player is farther into the block horizontal
                    } else {
                        // if the player is not in the position to slide across the gap between blocks: 
                        if(slideX == false)
                        {
                            // hitting a wall so store the time
                            wallTime = System.currentTimeMillis();
                            // stop the player from travelling into the block farther
                            p1.setXVel(0);
                            // if the player is colliding with the right of the block
                            if(x < i) // right
                            {
                                // move the player out of the block horizontally left
                                p1.setX(p1.getX() - overX);
                                // set the player's state to be on a (right) wall
                                p1.setState(Player.State.ONRIGHTWALL);
                            } else {
                                // move the player out of the block horizontally right
                                p1.setX(p1.getX() + overX);
                                // set the player's state to be on a (right) wall
                                p1.setState(Player.State.ONLEFTWALL);
                            }
                        }
                    }
                }
            }
        }
    }

    public void update(float delta) {
        // update the local player using the change in time
        p1.update(delta);
        // update the simulated player as if no time has pasted
        // this makes the player's death tick tick
        p2.update(0);
        // make the local player move
        processInput();
        // if the game is running the host side: 
        if (isHost) {
            // if a player has just joined and needs the colour of th ehosting player
            if(server.getResponseRequired())
            {
                // send the player's colour
                server.initialize(p1);
                // respwn the players
                server.respawn();
                // respawn the host player
                p1.respawn(level.respawnPlayer(1, 0), level.respawnPlayer(1, 1));
            }
            // get the guest player's colour
            p2.setColour(server.retrieveColour());
            // send the hosting player's info
            server.SendPlayer(p1);
            // if the guest player was not shielding and is now shielding: 
            if(p2.getAction() != Player.Action.SHIELD && server.getGlobalPlayer().getAction() == Player.Action.SHIELD)
                // play the shield sound play
                shieldSound.play(0.5f);
            // get all the info on the guest player from the server
            p2.setX(server.getGlobalPlayer().getX());
            p2.setY(server.getGlobalPlayer().getY());
            p2.setState(server.getGlobalPlayer().getState());
            p2.setDirection(server.getGlobalPlayer().getDirection());
            p2.setAction(server.getGlobalPlayer().getAction());
            p2.setDeathTick(server.getGlobalPlayer().getDeathTick());
            // if the server says the guest hit the host
            if(server.getAction().equals("killHost"))
            {
                // freeze the host player where it was hit
                p1.setX(server.getLocalPlayer().getX());
                p1.setY(server.getLocalPlayer().getY());
                // kill the player
                p1.kill();
            }
            // if one of the players have died
            if(p1.getDeathTick() == 0 || p2.getDeathTick() == 0)
            {
                // ignore any requests from the guest
                server.setAction("doNothing");
                // send a message to the guest saying to respawn
                server.respawn();
                // respawn the host player in the coordinates of the host
                p1.respawn(level.respawnPlayer(1, 0), level.respawnPlayer(1, 1));
            }
        } else {
            // get the host player's colour
            p2.setColour(client.getGlobalPlayer().getColor());
            // snd the guest player's info to the host
            client.SendPlayer(p1);
            // if the guest player was not shielding and is now shielding: 
            if(p2.getAction() != Player.Action.SHIELD && client.getGlobalPlayer().getAction() == Player.Action.SHIELD)
                shieldSound.play(0.5f);
            // get all the info on the host player from the client
            p2.setX(client.getGlobalPlayer().getX());
            p2.setY(client.getGlobalPlayer().getY());
            p2.setState(client.getGlobalPlayer().getState());
            p2.setDirection(client.getGlobalPlayer().getDirection());
            p2.setAction(client.getGlobalPlayer().getAction());
            p2.setDeathTick(client.getGlobalPlayer().getDeathTick());
            // if the client says the guest hit the host
            if(client.getAction().equals("killGuest"))
            {
                // freeze the guest player where it was hit
                p1.setX(client.getLocalPlayer().getX());
                p1.setY(client.getLocalPlayer().getY());
                // kill the guest
                p1.kill();
            }
            // if the server has send a message to respawn and the client says to do so
            if(client.getAction().equals("respawn"))
            {
                // stop the client from saying to kill the guest again
                client.setAction("doNothing");
                // respawn the guest player in the coordinates of the guest
                p1.respawn(level.respawnPlayer(2, 0), level.respawnPlayer(2, 1));
            }
        }
        if(!(p1.getAction() == Player.Action.ATTACK && p2.getAction() == Player.Action.ATTACK))
        {
            // create a vector to represent the repulsive force using the difference in the x and y of both players
            Vector2 repulsiveForce = new Vector2(p1.getX() - p2.getX(), p1.getY() - p2.getY());
            // scale the vector by the inverse of the length of the vector
            // this is effectively changing the length of the vector because the first term is canceled when multiplied with the vector
            repulsiveForce.scl((float)(1/(Math.pow(repulsiveForce.len(), 3))));
            // if the vector is not NaN (not a number): 
            if(!Float.isNaN(repulsiveForce.len()))
            {
                // the number of maximum velocities the player can be pushed around by
                int c = 4;
                // if the magnitude of the repulsive force is greater than the Max # of velocities
                if(Math.abs(repulsiveForce.x) > c * Player.MAX_VELOCITY)
                    // if the x component is positive
                    if(repulsiveForce.x > 0)
                        // limit the repulsive force to the positive max # of velocities
                        repulsiveForce.x = c * Player.MAX_VELOCITY;
                    else
                        // limit the repulsive force to the negative max # of velocities
                        repulsiveForce.x = -c * Player.MAX_VELOCITY;
                // if the x component of the force is very small
                if(Math.abs(repulsiveForce.x) < Player.MAX_VELOCITY/100)
                    // make it zero
                    repulsiveForce.x = 0;
                // do the same to the y component
                if(Math.abs(repulsiveForce.y) > c * Player.MAX_VELOCITY)
                    if(repulsiveForce.y > 0)
                        repulsiveForce.y = c * Player.MAX_VELOCITY;
                    else
                        repulsiveForce.y = -c * Player.MAX_VELOCITY;
                if(Math.abs(repulsiveForce.y) < Player.MAX_VELOCITY/100)
                    repulsiveForce.y = 0;
                
                // add the repulsive force to the player's speed (accelerating in that direction)
                p1.setXVel(p1.getXVel() + repulsiveForce.x);
                p1.setYVel(p1.getYVel() + repulsiveForce.y);
                
                // if the player is pushed outside the level put it back in
                if(p1.getX() < 1)
                    p1.setX(1);
                if(p1.getY() < 1)
                    p1.setY(1);
                if(p1.getX() > Level.WIDTH - 1)
                    p1.setX(Level.WIDTH - 1);
                if(p1.getY() > Level.HEIGHT - 1)
                    p1.setY(Level.HEIGHT - 1);
            }
        }
        
        // when exit is true, then
        if(exit == true) 
        {
            Gdx.app.exit();             // can exit game screen (or application)
        }
    }  
    @Override
    public boolean keyDown(int keycode) {
        
        // When player presses and holds the keys
        
        if(keycode == Keys.A)       // move the player to the left(A)
            left = true;
        if(keycode == Keys.D)       // move the player to the right(D)
            right = true;
        if(keycode == Keys.W)       // the player presses up(W)
            up = true;
        if(keycode == Keys.S)       // the player presses down(S)
            down = true;
        if(keycode == Keys.SPACE)   // the player presses space(SPACE)
            jump = true;
        if(keycode == Keys.P) {     
            // player can only attack when stamina is between 33 and 100 percent
            if(p1.getStamina() >= 66 && p1.getStamina() <= 100)
            {
                // if direction is not 'STILL' and action is not 'ATTACK'
                if(p1.getDirection() != Player.Direction.STILL && p1.getAction() != Player.Action.ATTACK)
                {
                    p1.setStamina(p1.getStamina() - 100);    // substract all stamina when attacking
                    attack = true;
                }
            }
        }
        // the player presses O (shield)
        if(keycode == Keys.O)       
            // Player can only shield when: Player chooses direction, shieldOn is true and there is no delay
            if(p1.getDirection() != Player.Direction.STILL && shieldOn==true && delayShield == false) 
            {
                shield = true;                  // player is shielding
                shieldTemp = shield;            // keep track if shield is true (using temp. variable)
            }
        if(keycode == Keys.ESCAPE)              // player presses ESCAPE
        {
            exit = true;                        // player has pressed escape, set exit to true
        }
        return true;
    }
        
    

    @Override
    public boolean keyUp(int keycode) {
        
        // When player releases on keys
        
        if(keycode == Keys.A)       // the player releases left 
            left = false;
        if(keycode == Keys.D)       // the player releases right
            right = false;
        if(keycode == Keys.W)       // the player releases up
            up = false; 
        if(keycode == Keys.S)       // the player releases down
            down = false;
        if(keycode == Keys.SPACE)   // the player releases jump
            jump = false;
        if(keycode == Keys.P)
            if(p1.getAction() != Player.Action.ATTACK)  // when player action is not 'ATTACK'
                attack = false;                         // player is not attacking
        if(keycode == Keys.O) 
        {
            shield = false;             // player is not shielding
            shieldTemp = shield;        // keep track if shield is false (using temp. variable)
        }
        if(keycode == Keys.ESCAPE)      // the player releases ESCAPE
        {
            exit = false;               
        }         
        return false;
    }
    

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override    
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
