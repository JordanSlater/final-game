/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fcs.Final;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 *
 * @author slatj2095
 */
public class LevelRenderer {
    private Level theLevel;
    private Player p1;
    private Player p2;
    private World world;
    
    //camera/sprite/shape variables
    private OrthographicCamera camera;
    private OrthographicCamera hud;
    private ShapeRenderer shapes;
    PolygonSprite poly;
    PolygonSpriteBatch polyBatch;
    Texture textureSolid;
    
    //screen dimensions variables
    public static float width;
    public static float height;
    
    //blacklight variables
    private final RayHandler rayHandler;
    private PointLight light;
    
    public LevelRenderer(Level level)
    {
        //constructor
        shapes = new ShapeRenderer();
        theLevel = level;
        p1 = level.getPlayer1();
        p2 = level.getPlayer2();
        camera = new OrthographicCamera(Level.WIDTH-1,Level.HEIGHT-1);
        camera.position.set(Level.WIDTH/2.0f,Level.HEIGHT/2.0f,0);
        camera.update();
        hud = new OrthographicCamera(Level.WIDTH-1,Level.HEIGHT-1);
        hud.position.set(Level.WIDTH/2.0f,Level.HEIGHT/2.0f,0);
        hud.update();
        world = new World(new Vector2(0, 0), true);
        for(int row = 0; row < Level.HEIGHT; row++)
        {
            for(int col = 0; col < Level.WIDTH; col++)
            {
                if(theLevel.getBlock(row, col) != null)
                {
                    PolygonShape b = new PolygonShape();
                    b.setAsBox(Block.SIZE/2, Block.SIZE/2);
                    BodyDef boxBodyDef = new BodyDef();
                    boxBodyDef.type = BodyType.StaticBody;
                    boxBodyDef.position.set(col+0.5f, row+0.5f);
                    Body boxBody = world.createBody(boxBodyDef);
                    boxBody.createFixture(b, 100f);
                }
            }
        }
        rayHandler = new RayHandler(world);
        // PointLight(rayHandler, 1000, new Color(0.00001f,0.00001f,0.00001f,1f), 50, 0, 0)
        light = new PointLight(rayHandler, 1000, new Color(0.00001f,0.00001f,0.00001f,1f), 50, 0, 0);
        light.setSoftnessLength(1f);
        light.setXray(false);                   // Ensure player cannot see beyond walls (shadow effect)
        rayHandler.setShadows(true);            // 'BLACKOUT Effect', casts shadow 
    }

    public void render(float delta) {     
        shapes.setProjectionMatrix(camera.combined);
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapes.begin(ShapeType.Filled);
        shapes.setColor(Color.BLACK);
        for(int row = 0; row < Level.HEIGHT; row++)
        {
            for(int col = 0; col < Level.WIDTH; col++)
            {
                if(theLevel.getBlock(row, col) != null)
                {
                    shapes.rect(col, row, Block.SIZE, Block.SIZE);
                }
            }
        }
        // end drawing blocks
        
        // drawing the second player
        if(p2 != null)
        {
            shapes.setColor(p2.getColor().r, p2.getColor().g, p2.getColor().b, p2.getDeathTick());
            if(p2.getAction() != Player.Action.ATTACK)
            {
                if(p2.getState() == Player.State.JUMPING)
                {
                    shapes.circle(p2.getX()+Player.SIZE/2, p2.getY()+Player.SIZE/2, Player.SIZE/2, 100);
                } else {
                    shapes.rect(p2.getX(), p2.getY(), Player.SIZE, Player.SIZE);
                }
            } else {
                shapes.identity();
                shapes.translate(p2.getX()+Player.SIZE/2, p2.getY()+Player.SIZE/2, 0);
                shapes.rotate(0, 0, 1f, p2.getDegrees());
                shapes.triangle(-Player.SIZE/2, -Player.SIZE/2, Player.SIZE/2, -Player.SIZE/2, 0, Player.SIZE/2-0.1f);
                shapes.identity();
            }
            if(p2.getAction()==Player.Action.SHIELD)
            {
                shapes.setColor(Color.CYAN);
                //draw player's shield
                if(p2.getState()!=Player.State.DYING)//shield only when not dying
                {
                    shapes.identity();
                    shapes.translate(p2.getX()+Player.SIZE/2, p2.getY()+Player.SIZE/2, 0);
                    shapes.rotate(0, 0, 1f, p2.getDegrees());
                    shapes.rect(-Player.LENGTH_SHIELD/2, Player.RADIUS_SHIELD, Player.LENGTH_SHIELD, Player.WIDTH_SHIELD);
                    shapes.identity();
                }
            }
        }
        
        // 'BLACKOUT' effect
        shapes.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
        
        float x = (float)(p1.getX()+Player.SIZE/2.0f);
        float y = (float)(p1.getY()+Player.SIZE/2.0f);
        light.setPosition(x,y);
        light.setColor(light.getColor().r, light.getColor().g, light.getColor().b, p1.getDeathTick());
        rayHandler.setCombinedMatrix(camera.combined);
        rayHandler.updateAndRender();
        
        shapes.setProjectionMatrix(hud.combined);
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapes.begin(ShapeType.Filled);
        
        // converts player's stamina to a percentage
        double percentage = 1.0*(p1.getStamina()/100);
        // drawing the stamina bar for the player         
        shapes.setColor(Color.GRAY);        // setColor(float r, float g, float b, float alpha component)
        shapes.rect(5, 0.3f, 10, 0.5f);     // the floats have to be in range of [0, 1]; so decimals      
        shapes.setColor(0f, 0.8f, 0f, 1f);
        shapes.rect(5, 0.3f, (float)(10*percentage), 0.5f); // refits stamina bar to player's stamina
        
        // drawing the player
        shapes.setColor(p1.getColor().r, p1.getColor().g, p1.getColor().b, p1.getDeathTick());
        if(p1.getAction() != Player.Action.ATTACK)
        {
            if(p1.getState() == Player.State.JUMPING)       // player is jumping
            {
                // turn the player into a cirlce
                shapes.circle(p1.getX()+Player.SIZE/2, p1.getY()+Player.SIZE/2, Player.SIZE/2, 100);
            } else {    // else (if the player is not jumping)
                // turn the player into a block
                shapes.rect(p1.getX(), p1.getY(), Player.SIZE, Player.SIZE);
            }
        } else {
            shapes.identity();
            shapes.translate(p1.getX()+Player.SIZE/2, p1.getY()+Player.SIZE/2, 0);
            shapes.rotate(0, 0, 1f, p1.getDegrees());
            float verticalDown = -(float)(Player.SIZE/2*Math.sin(Math.toRadians(30)));
            float verticalUp = (float)(Player.SIZE*Math.sin(Math.toRadians(60)) + verticalDown);
            shapes.triangle(-Player.SIZE/2, verticalDown, Player.SIZE/2, verticalDown, 0, verticalUp);
            shapes.identity();
        }
        
        // if the player is shielding
        if(p1.getAction()==Player.Action.SHIELD)
        {
            shapes.setColor(Color.CYAN);    // colour of shield
            //draw player's shield
            if(p1.getState()!=Player.State.DYING) //shield only when not dying
            {
                shapes.identity();
                shapes.translate(p1.getX()+Player.SIZE/2, p1.getY()+Player.SIZE/2, 0);  // position of shield
                shapes.rotate(0, 0, 1f, p1.getDegrees());           // shield can be rotated
                shapes.rect(-Player.LENGTH_SHIELD/2, Player.RADIUS_SHIELD, Player.LENGTH_SHIELD, Player.WIDTH_SHIELD); // draw the shield
                shapes.identity();
            }
        }
        
        shapes.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }
     
    public void setSize(int width, int height) {
        
        this.width = width; //set the width
        this.height = height; //set the height
    }

//    @Override
    public void show() {
        
    }

//    @Override
    public void hide() {
        
    }

//    @Override
    public void pause() {
        
    }

//    @Override
    public void resume() {
        
    }

//    @Override
    public void dispose() {
        world.dispose(); //dispose of world
    }
}
